INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

--taylor swift #5
--for fearless album #2

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", "00:04:06", "Pop rock", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", "00:03:06", "Country pop", 2);

--Create a new album for taylor swift
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-22", 5);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", "00:04:33", "Rock, alternative rock, arena rock", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", "00:03:24", "Country", 7);

--lady gaga artists
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Start is Born", "2018-10-05", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", "00:03:01", "Rock and roll", 13);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", "00:03:21", "Country, rock, folk rock", 13);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Start is Born", "2018-10-05", 8);